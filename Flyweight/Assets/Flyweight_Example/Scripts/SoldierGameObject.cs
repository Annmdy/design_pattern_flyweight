using UnityEngine;
using System.Collections;
using FlyweightExample;

// This is the Heavyweight of the Flyweight Pattern
public class SoldierGameObject : MonoBehaviour
{
    private ISoldierFlyweight soldier = null;
    public int health = 0;

    
    public void Create(SoldierType soldierType)
    {
        soldier = SoldierFlyweightFactory.Soldier(soldierType);
        gameObject.AddComponent<BoxCollider>();
    }

    // sets mouse cursor on false
    bool mouseOver = false;
    void OnMouseOver()
    { 
        // if mouse cursor hovers over the GameObject it
        mouseOver = true;
    }
    void OnMouseExit()
    {
        mouseOver = false;
    }

    // ____________________ !EXTRA! BUT NOT NECESSARRY !________________________
    // This is only for the purpose of viewing the info/stats of the diffirent soldiers
    void OnGUI()
    {
        if (!mouseOver)
            return;
        if (soldier == null)
            return;

        string soldierInfo = string.Format(
            "Type: {0}\nWeapon: {1}\nAttack: {2}\nHealth: {3}\nDefense: {4}",
            
            soldier.Type,
            soldier.Stats.Weapon,
            soldier.Stats.Attack,
            soldier.Stats.Health,
            soldier.Stats.Defense
        );
        GUI.Label(new Rect(0, 50, 1000, 1000), soldierInfo);
    }
}
