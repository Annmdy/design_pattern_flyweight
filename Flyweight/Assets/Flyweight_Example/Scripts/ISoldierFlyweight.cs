using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


namespace FlyweightExample

// This Interface contains the class stats,enum and color method for the Soldiears
{
    interface ISoldierFlyweight
    {
        SoldierStats Stats { get;}
        SoldierType Type { get; }
        Color GetColor(int color);
    }

    public enum SoldierType
    {
        Archer,
        Knight, 
        Cavalary
    }

    class SoldierStats
    {
        public string Weapon;
        public int Attack;
        public int Defense;
        public int Health;
    }
}
