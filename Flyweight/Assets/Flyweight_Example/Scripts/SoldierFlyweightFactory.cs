using System;
using System.Collections.Generic;
using System.Text;

// This is the FlyweightFactory of the Flyweight Pattern

namespace FlyweightExample
{
    static class SoldierFlyweightFactory
    {
        private static Dictionary<SoldierType, ISoldierFlyweight> Soldiers = new Dictionary<SoldierType, ISoldierFlyweight>();
        public static int SoldierCount { get; private set; }

        public static ISoldierFlyweight Soldier(SoldierType weaponType)
        {
            // Flyweight Factory use of Key
            if (!Soldiers.ContainsKey(weaponType))
            {
                switch (weaponType)
                {
                    case SoldierType.Knight:
                        Soldiers.Add(weaponType, new TypeKnight());
                        break;
                    case SoldierType.Archer:
                        Soldiers.Add(weaponType, new TypeArcher());
                        break;
                    case SoldierType.Cavalary:
                    default:
                        Soldiers.Add(weaponType, new TypeCalvary());
                        break;
                }

                SoldierCount++;
            }

            return Soldiers[weaponType];
        }

    }
}
