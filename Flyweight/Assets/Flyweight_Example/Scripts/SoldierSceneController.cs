using UnityEngine;
using System.Collections;
using FlyweightExample;

// This is the Client of the Flyweight Pattern

// This class is responsible for making visual game objects.
// The creation of the SoldierGameObjects is handled here.

public class SoldierSceneController : MonoBehaviour
{
    // flyweight count will be 3. Thus reducing memory overhead for handling large amounts of soldiers.
    
    // This is for the Inspector Menu
    // Here you can choose how many Soldiars you want to create
    // and in what position range they should be
    public int numberOfSoldiersToCreate;
    public Vector2 minPosition;
    public Vector2 maxPosition;
    
    void Start()
    {
        int weaponType = 0;
        for (int i = 0; i < numberOfSoldiersToCreate; i++)
        {
            // Creates a Soldier GameObject Cube and the size
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Cube);
            go.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
            
            SoldierGameObject soldierGameObject = go.AddComponent<SoldierGameObject>();
            soldierGameObject.Create((SoldierType)weaponType);
            go.name = ((SoldierType)weaponType).ToString();
            int startHealth = SoldierFlyweightFactory.Soldier((SoldierType)weaponType).Stats.Health;
            soldierGameObject.health = startHealth;

            // random soldier positions
            float x = Random.Range(minPosition.x, maxPosition.x);
            float y = Random.Range(minPosition.y, maxPosition.y);
            go.transform.position = new Vector3(x, 0.45f * go.transform.localScale.y, y);

            go.GetComponent<MeshRenderer>().material.color = SoldierFlyweightFactory.Soldier((SoldierType)weaponType).GetColor(startHealth);
            weaponType = (weaponType + 1) % 3;
        }
    }
}
