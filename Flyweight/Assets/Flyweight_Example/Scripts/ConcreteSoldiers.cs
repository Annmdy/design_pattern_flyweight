using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

// This is the ConcreteFlyweight of the Flyweight Pattern

namespace FlyweightExample

// This is were we safe the data of the soldiars that are created
// Type/Name
// Stats (class in the Interface ISoldierFlyweight)
// Weapon (enum in the Interface ISoldierFlyweight)
// and color (method from the Interface ISoldierFlyweight)

{
    // _________________________ Archer stats ____________________________
    class TypeArcher : ISoldierFlyweight
    {
        // stats are null
        private SoldierStats stats;
        public SoldierStats Stats
        {
            get
            {
                stats = new SoldierStats()
                {
                    Weapon = "Bow",
                    Attack = 15,
                    Defense = 4,
                    Health = 20,
                };
                return stats;
            }
        }
        public SoldierType Type { get; }

        public SoldierType Weapon
        {
            get
            {
                return SoldierType.Archer;
            }
        }
        public Color GetColor(int color)
        {
                return Color.green;
        }
    }
    
    // _________________________ Knight stats ____________________________
    class TypeKnight : ISoldierFlyweight
    {
        // stats are null
        private SoldierStats stats;

        public SoldierStats Stats
        {
            get
            {
                stats = new SoldierStats()
                {
                    Weapon = "Sword",
                    Attack = 25,
                    Defense = 20,
                    Health = 30,
                }; 
                return stats;
            }
        }
        public SoldierType Type
        {
            get
            {
                return SoldierType.Knight;
            }
        }
        public Color GetColor(int color)
        {
            return Color.blue;
        }
    }
    
    // _________________________ Calvary stats ____________________________
    class TypeCalvary : ISoldierFlyweight
    {
        // stats are null
        private SoldierStats stats;

        
        public SoldierStats Stats
        {
            get
            {
                stats = new SoldierStats()
                {
                    Weapon = "Calvary",
                    Attack = 30,
                    Defense = 20,
                    Health = 25,
                };
                return stats;
            }
        }
        public SoldierType Type
        {
            get
            {
                return SoldierType.Cavalary;
            }
        }
        public Color GetColor(int color)
        {
            return Color.yellow;
        }
    }
}
