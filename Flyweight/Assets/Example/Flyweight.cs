﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlyweightPattern
{
    public class Flyweight : MonoBehaviour
    {
        //The list that stores all magicians!
        List<Magician> allMagicians = new List<Magician>();
        
        public List<Vector3> potionPosition;
        public List<Vector3> armPosition;
        public List<Vector3> legPosition;

        private void Start()
        {
            //List used when we enable flyweight
            potionPosition = GetPartPositions();
            armPosition = GetPartPositions();
            legPosition = GetPartPositions();
            
            //Lets create all magicians!
            for (int i = 0; i < 366; i++)
            {
                Magician newMagician = new Magician();

                //Add potion and bodypart positions here
                
                //Without flyweight
                newMagician.potionPosition = GetPartPositions();
                newMagician.armPosition = GetPartPositions();
                newMagician.legPosition = GetPartPositions();

                //With flyweight
                newMagician.potionPosition = potionPosition;
                newMagician.armPosition = armPosition;
                newMagician.legPosition = legPosition;

                allMagicians.Add(newMagician);
            }
        }
        
        //Generate a list with all the part positions
        List<Vector3> GetPartPositions()
        {
            //Create a new list
            List<Vector3> partPositions = new List<Vector3>();

            //Add body part positions to the list
            for (int i = 0; i < 1000; i++)
            {
                partPositions.Add(new Vector3());
            }

            return partPositions;
        }
    }
    
    
}
