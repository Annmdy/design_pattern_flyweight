﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace FlyweightPattern
{ //Class that includes the Body parts and the potion of all the wizards
    public class Magician
    {
        public List<Vector3> potionPosition;
        public List<Vector3> armPosition;
        public List<Vector3> legPosition;
    }
}